{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "warranty-status.name" }}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this
(by the DNS naming spec).
*/}}
{{- define "warranty-status.fullname" }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Calculate api certificate
*/}}
{{- define "warranty-status.api-certificate" }}
{{- if (not (empty .Values.ingress.api.certificate)) }}
{{- printf .Values.ingress.api.certificate }}
{{- else }}
{{- printf "%s-api-letsencrypt" (include "warranty-status.fullname" .) }}
{{- end }}
{{- end }}

{{/*
Calculate api hostname
*/}}
{{- define "warranty-status.api-hostname" }}
{{- if (and .Values.config.api.hostname (not (empty .Values.config.api.hostname))) }}
{{- printf .Values.config.api.hostname }}
{{- else }}
{{- if .Values.ingress.api.enabled }}
{{- printf .Values.ingress.api.hostname }}
{{- else }}
{{- printf "%s-api" (include "warranty-status.fullname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate api base url
*/}}
{{- define "warranty-status.api-base-url" }}
{{- if (and .Values.config.api.baseUrl (not (empty .Values.config.api.baseUrl))) }}
{{- printf .Values.config.api.baseUrl }}
{{- else }}
{{- if .Values.ingress.api.enabled }}
{{- $hostname := ((empty (include "warranty-status.api-hostname" .)) | ternary .Values.ingress.api.hostname (include "warranty-status.api-hostname" .)) }}
{{- $protocol := (.Values.ingress.api.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "warranty-status.api-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}


{{/*
Calculate fe certificate
*/}}
{{- define "warranty-status.fe-certificate" }}
{{- if (not (empty .Values.ingress.fe.certificate)) }}
{{- printf .Values.ingress.fe.certificate }}
{{- else }}
{{- printf "%s-fe-letsencrypt" (include "warranty-status.fullname" .) }}
{{- end }}
{{- end }}

{{/*
Calculate fe hostname
*/}}
{{- define "warranty-status.fe-hostname" }}
{{- if (and .Values.config.fe.hostname (not (empty .Values.config.fe.hostname))) }}
{{- printf .Values.config.fe.hostname }}
{{- else }}
{{- if .Values.ingress.fe.enabled }}
{{- printf .Values.ingress.fe.hostname }}
{{- else }}
{{- printf "%s-fe" (include "warranty-status.fullname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate fe base url
*/}}
{{- define "warranty-status.fe-base-url" }}
{{- if (and .Values.config.fe.baseUrl (not (empty .Values.config.fe.baseUrl))) }}
{{- printf .Values.config.fe.baseUrl }}
{{- else }}
{{- if .Values.ingress.fe.enabled }}
{{- $hostname := ((empty (include "warranty-status.fe-hostname" .)) | ternary .Values.ingress.fe.hostname (include "warranty-status.fe-hostname" .)) }}
{{- $protocol := (.Values.ingress.fe.tls | ternary "https" "http") }}
{{- printf "%s://%s" $protocol $hostname }}
{{- else }}
{{- printf "http://%s" (include "warranty-status.fe-hostname" .) }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Calculate postgres url
*/}}
{{- define "api.postgres-url" }}
{{- $postgres := .Values.config.postgres }}
{{- if $postgres.url }}
{{- printf $postgres.url }}
{{- else }}
{{- $credentials := ((or (empty $postgres.username) (empty $postgres.password)) | ternary "" (printf "%s:%s@" $postgres.username $postgres.password)) }}
{{- printf "postgresql://%s%s:%s/%s" $credentials $postgres.host $postgres.port $postgres.database }}
{{- end }}
{{- end }}
